<?php

namespace Ecom\Api\Helper;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\QuoteFactory;
use Magento\Quote\Model\QuoteManagement;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Model\Order\Payment\Transaction;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;
use Magento\Sales\Model\Service\OrderService;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class Data
 * @package Ecom\Api\Helper
 */
class Data extends AbstractHelper
{
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var CustomerFactory
     */
    protected $customerFactory;
    /**
     * @var QuoteFactory
     */
    protected $quote;
    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;
    /**
     * @var QuoteManagement
     */
    protected $quoteManagement;
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    /**
     * @var OrderService
     */
    protected $orderService;
    /**
     * @var BuilderInterface
     */
    protected $transactionBuilder;

    /**
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param QuoteFactory $quote
     * @param QuoteManagement $quoteManagement
     * @param CustomerFactory $customerFactory
     * @param CustomerRepositoryInterface $customerRepository
     * @param OrderService $orderService
     * @param ProductRepositoryInterface $productRepository
     * @param BuilderInterface $transactionBuilder
     */
    public function __construct(
        Context $context,
        StoreManagerInterface $storeManager,
        QuoteFactory $quote,
        QuoteManagement $quoteManagement,
        CustomerFactory $customerFactory,
        CustomerRepositoryInterface $customerRepository,
        OrderService $orderService,
        ProductRepositoryInterface $productRepository,
        BuilderInterface $transactionBuilder
    ) {
        $this->storeManager = $storeManager;
        $this->quote = $quote;
        $this->quoteManagement = $quoteManagement;
        $this->customerFactory = $customerFactory;
        $this->customerRepository = $customerRepository;
        $this->orderService = $orderService;
        $this->productRepository = $productRepository;
        $this->transactionBuilder = $transactionBuilder;
        parent::__construct($context);
    }

    /**
     * Create Order
     *
     * @param array $orderData
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function createOrder(array $orderData)
    {
        $store = $this->storeManager->getStore();
        $websiteId = $this->storeManager->getStore()->getWebsiteId();
        $quote = $this->quote->create();

        $customer = $this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        $customer->loadByEmail($orderData['customer']['email']);

        if ($customer->getEntityId()) {
            $_customer = $this->customerRepository->getById($customer->getEntityId());
            $quote->assignCustomer($_customer);
        }

        // Create as a guest if customer not exists
        if (!$customer->getEntityId()) {
            $quote->setCustomerEmail($orderData['customer']['email']);
            $quote->setCustomerIsGuest(true);
        }

        $quote->setStore($store);
        $quote->setCurrency();

        foreach ($orderData['products'] as $item) {
            $product = $this->productRepository->get($item['sku']);
            $quote->addProduct($product, (int)$item['qty']);
        }

        $quote->getBillingAddress()->addData($orderData['billing_address']);
        $quote->getShippingAddress()->addData($orderData['shipping_address']);

        // Collect rates and set shipping and payment method
        $shippingAddress = $quote->getShippingAddress();
        $shippingAddress->setCollectShippingRates(true)
            ->collectShippingRates()
            ->setShippingMethod($orderData['shipping_method_code'] . '_' . $orderData['shipping_carrier_code']);

        $quote->setPaymentMethod($orderData['payment_method']);
        $quote->setInventoryProcessed(false);
        $quote->save();

        // Set sales order payment
        $quote->getPayment()->importData(['method' => $orderData['payment_method']]);

        // Collect totals and save quote
        $quote->collectTotals()->save();

        // Create order from quote
        $order = $this->quoteManagement->submit($quote);

        if (!$order->getEntityId()) {
            throw new LocalizedException(__('Order not has been created.'));
        }

        $order->setEmailSent(0);

        // Set transaction ID
        if (isset($orderData['transaction_id'])) {
            $this->createTransaction($order, ['id' => (int)$orderData['transaction_id']]);
        }

        return $order->getRealOrderId();
    }

    /**
     * @param $order
     * @param array $paymentData
     * @return mixed
     */
    public function createTransaction(OrderInterface $order, array $paymentData)
    {
        $payment = $order->getPayment();
        $payment->setLastTransId($paymentData['id']);
        $payment->setTransactionId($paymentData['id']);
        $payment->setAdditionalInformation(
            [Transaction::RAW_DETAILS => (array) $paymentData]
        );
        $formattedPrice = $order->getBaseCurrency()->formatTxt(
            $order->getGrandTotal()
        );

        $message = __('The authorized amount is %1.', $formattedPrice);

        $trans = $this->transactionBuilder;
        $transaction = $trans->setPayment($payment)
            ->setOrder($order)
            ->setTransactionId($paymentData['id'])
            ->setAdditionalInformation(
                [Transaction::RAW_DETAILS => (array) $paymentData]
            )
            ->setFailSafe(true)
            ->build(Transaction::TYPE_CAPTURE);

        $payment->addTransactionCommentsToOrder(
            $transaction,
            $message
        );
        $payment->setParentTransactionId(null);
        $payment->save();
        $order->save();

        return $transaction->save()->getTransactionId();
    }
}
