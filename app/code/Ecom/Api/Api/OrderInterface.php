<?php

namespace Ecom\Api\Api;

interface OrderInterface
{
    /**
     * @return mixed
     */
    public function create();
}
