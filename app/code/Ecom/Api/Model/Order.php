<?php

namespace Ecom\Api\Model;

use Ecom\Api\Api\OrderInterface;
use Ecom\Api\Helper\Data;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Webapi\Rest\Request;

class Order implements OrderInterface
{
    /**
     * @var Request
     */
    protected $request;
    /**
     * @var Data
     */
    protected $helper;

    /**
     * Order constructor
     *
     * @param Request $request
     * @param Data $helper
     */
    public function __construct(
        Request $request,
        Data $helper
    ) {
        $this->request = $request;
        $this->helper = $helper;
    }

    /**
     * @return mixed|void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function create()
    {
        $params = $this->request->getBodyParams();
        $this->validate($params);
        $orderId = $this->helper->createOrder($params);

        $response = ['order_id' => $orderId];
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    /**
     * @param $params
     * @throws LocalizedException
     */
    protected function validate($params)
    {
        if (!isset($params['products'])) {
            throw new LocalizedException(__('Not found "products" field.'));
        }

        if (!is_array($params['products']) || empty($params['products'])) {
            throw new LocalizedException(__('Field "products" should be as array and can not be empty.'));
        }

        foreach ($params['products'] as $key=>$product) {
            if (!isset($product['sku'])) {
                throw new LocalizedException(__('Not found "products.sku.' . $key . '" field.'));
            }
            if (!isset($product['qty'])) {
                throw new LocalizedException(__('Not found "products.qty.' . $key . '" field.'));
            }
        }

        if (!isset($params['billing_address'])) {
            throw new LocalizedException(__('Not found "billing_address" field.'));
        }

        if (!isset($params['shipping_address'])) {
            throw new LocalizedException(__('Not found "shipping_address" field.'));
        }

        if (!isset($params['customer'])) {
            throw new LocalizedException(__('Not found "customer" field.'));
        }

        if (!isset($params['customer']['email'])) {
            throw new LocalizedException(__('Not found "customer.email" field.'));
        }

        if (!isset($params['shipping_method_code'])) {
            throw new LocalizedException(__('Not found "shipping_method_code" field.'));
        }

        if (!isset($params['shipping_carrier_code'])) {
            throw new LocalizedException(__('Not found "shipping_carrier_code" field.'));
        }
    }
}
